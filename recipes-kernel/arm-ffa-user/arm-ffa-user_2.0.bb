SUMMARY = "Thin layer to expose FF-A operations towards user space"
DESCRIPTION = "${SUMMARY}"
LICENSE =  "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=05e355bbd617507216a836c56cf24983"

inherit module

SRC_URI = "git://gitlab.arm.com/linux-arm/linux-trusted-services;protocol=https;branch=main \
           file://0001-Add-UUID-of-Internal-Trusted-Storage.patch \
           file://Makefile;subdir=git \
          "
S = "${WORKDIR}/git"

SRCREV = "06a0228a8edee804e6a8ed02f02c0b9e6e7d1057"

PV = "2.0+git${SRCPV}"

RPROVIDES:${PN} += "kernel-module-arm-ffa-user"

KERNEL_MODULE_AUTOLOAD += "arm-ffa-user"

FILES:${PN} += "/lib/modules/*"

do_install:append() {
    install -d ${D}${includedir}
    install -m 0644 ${S}/arm_ffa_user.h ${D}${includedir}/
}
