SUMMARY = "A Linux kernel module providing user space access to Trusted Services"
DESCRIPTION = "${SUMMARY}"
LICENSE =  "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=05e355bbd617507216a836c56cf24983"

inherit module

SRC_URI = "git://gitlab.arm.com/linux-arm/linux-trusted-services;protocol=https;branch=main \
           file://Makefile;subdir=git \
          "
S = "${WORKDIR}/git"

SRCREV = "6eab9191e172e992376b64b6b6d9aa2fcaec18bb"

RPROVIDES:${PN} += "kernel-module-arm-ffa-tee"

KERNEL_MODULE_AUTOLOAD += "arm-ffa-tee"

do_install:append() {
    install -d ${D}${includedir}
    install -m 0644 ${S}/uapi/arm_ffa_tee.h ${D}${includedir}/
}

FILES:${PN} = "/lib/modules/*"
