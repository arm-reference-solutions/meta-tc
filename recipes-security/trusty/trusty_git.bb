SUMMARY = "Trusty TEE"
DESCRIPTION = "Trusty is a secure Operating System (OS) that provides a Trusted Execution Environment (TEE) for Android"
HOMEPAGE = "https://source.android.com/security/trusty"

PACKAGE_ARCH = "${MACHINE_ARCH}"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://trusty/kernel/LICENSE;md5=1163d440b7d4a6defb52a783d445ff49"

PV = "1.0+git${SRCPV}"
SRCREV_FORMAT = "kernel"

TRUSTY_REPO_ROOT = "android.googlesource.com"

# Trusty Core Projects
SRC_URI:append = " ${SRC_URI_LK} \
                   ${SRC_URI_HEADERS} \
                   ${SRC_URI_MUSL} \
                   ${SRC_URI_USER_BASE} \
                   ${SRC_URI_KERNEL} \
                 "
SRC_URI_LK = "git://${TRUSTY_REPO_ROOT}/trusty/lk/common;protocol=https;branch=master;name=lk;destsuffix=git/external/lk"
SRC_URI_HEADERS = "git://${TRUSTY_REPO_ROOT}/trusty/external/headers;protocol=https;branch=master;name=headers;destsuffix=git/external/headers"
SRC_URI_MUSL = "git://${TRUSTY_REPO_ROOT}/trusty/external/musl;protocol=https;branch=master;name=musl;destsuffix=git/external/trusty/musl"
SRC_URI_USER_BASE = "git://${TRUSTY_REPO_ROOT}/trusty/lib;protocol=https;branch=master;name=user_base;destsuffix=git/trusty/user/base"
SRC_URI_KERNEL = "git://${TRUSTY_REPO_ROOT}/trusty/lk/trusty;protocol=https;branch=master;name=kernel;destsuffix=git/trusty/kernel"
SRCREV_lk = "24d44556bb5939dc007584c7d7b87490f5cff797"
SRCREV_headers = "fe7222b7341192abe23982374f4fc11061255965"
SRCREV_musl = "abbff000b920855cebbbed681c19ff7160f36002"
SRCREV_user_base = "db2aadc4f2f39e960b37f8681a780c22a41efd55"
SRCREV_kernel = "c7935db04b08df2da462e92ce224bb001c69fe8a"
# Apps
SRC_URI:append = " ${SRC_URI_CONFIRMATIONUI} \
                   ${SRC_URI_SAMPLE} \
                   ${SRC_URI_KEYMASTER} \
                   ${SRC_URI_GATEKEEPER} \
                   ${SRC_URI_STORAGE} \
                   ${SRC_URI_AVB} \
                 "
SRC_URI_CONFIRMATIONUI = "git://${TRUSTY_REPO_ROOT}/trusty/app/confirmationui;protocol=https;branch=master;name=confirmationui;destsuffix=git/trusty/user/app/confirmationui"
SRC_URI_SAMPLE = "git://${TRUSTY_REPO_ROOT}/trusty/app/sample;protocol=https;branch=master;name=sample;destsuffix=git/trusty/user/app/sample"
SRC_URI_KEYMASTER = "git://${TRUSTY_REPO_ROOT}/trusty/app/keymaster;protocol=https;branch=master;name=keymaster;destsuffix=git/trusty/user/app/keymaster"
SRC_URI_GATEKEEPER = "git://${TRUSTY_REPO_ROOT}/trusty/app/gatekeeper;protocol=https;branch=master;name=gatekeeper;destsuffix=git/trusty/user/app/gatekeeper"
SRC_URI_STORAGE = "git://${TRUSTY_REPO_ROOT}/trusty/app/storage;protocol=https;branch=master;name=storage;destsuffix=git/trusty/user/app/storage"
SRC_URI_AVB = "git://${TRUSTY_REPO_ROOT}/trusty/app/avb;protocol=https;branch=master;name=avb;destsuffix=git/trusty/user/app/avb"
SRCREV_confirmationui = "ac23680911ba26ea28aa08e1c7ef6053c3d900a7"
SRCREV_sample = "1d9655a3f11c69050f9aea58fb861527a08cb2fc"
SRCREV_keymaster = "355d3eca9242611954768f040e04c76361b2cdfb"
SRCREV_gatekeeper = "d3586e84c998fba6577f203c62b5405e1ea8d96a"
SRCREV_storage = "8e717675299c6f0e4e2ae6eeeaa70cf3a61e7599"
SRCREV_avb = "1e716c5d74d663fc76def356fc63f8a2ec764c2a"
# Platform Projects
SRC_URI:append = " ${SRC_URI_BORINGSSL} \
                   ${SRC_URI_FREETYPE} \
                   ${SRC_URI_GOOGLETEST} \
                   ${SRC_URI_LIBCPPBOR} \
                   ${SRC_URI_LIBCXX} \
                   ${SRC_URI_LIBCXXABI} \
                   ${SRC_URI_NANOPB_C} \
                   ${SRC_URI_OPEN_DICE} \
                   ${SRC_URI_PROTOBUF} \
                   ${SRC_URI_SCUDO} \
                   ${SRC_URI_LIBHARDWARE} \
                   ${SRC_URI_SYS_GATEKEEPER} \
                   ${SRC_URI_SYS_ATTESTATION} \
                   ${SRC_URI_SYS_KEYMASTER} \
                   ${SRC_URI_SYS_TEEUI} \
                 "
SRC_URI_BORINGSSL = "git://${TRUSTY_REPO_ROOT}/platform/external/boringssl;protocol=https;branch=master;name=boringssl;destsuffix=git/external/boringssl"
SRC_URI_FREETYPE = "git://${TRUSTY_REPO_ROOT}/platform/external/freetype;protocol=https;branch=master;name=freetype;destsuffix=git/external/freetype"
SRC_URI_GOOGLETEST = "git://${TRUSTY_REPO_ROOT}/platform/external/googletest;protocol=https;branch=master;name=googletest;destsuffix=git/external/googletest"
SRC_URI_LIBCPPBOR = "git://${TRUSTY_REPO_ROOT}/platform/external/libcppbor;protocol=https;branch=master;name=libcppbor;destsuffix=git/external/libcppbor"
SRC_URI_LIBCXX = "git://${TRUSTY_REPO_ROOT}/platform/external/libcxx;protocol=https;branch=master;name=libcxx;destsuffix=git/external/libcxx"
SRC_URI_LIBCXXABI = "git://${TRUSTY_REPO_ROOT}/platform/external/libcxxabi;protocol=https;branch=master;name=libcxxabi;destsuffix=git/external/libcxxabi"
SRC_URI_NANOPB_C = "git://${TRUSTY_REPO_ROOT}/platform/external/nanopb-c;protocol=https;branch=master;name=nanopb_c;destsuffix=git/external/nanopb-c"
SRC_URI_OPEN_DICE = "git://${TRUSTY_REPO_ROOT}/platform/external/open-dice;protocol=https;branch=master;name=open_dice;destsuffix=git/external/open-dice"
SRC_URI_PROTOBUF = "git://${TRUSTY_REPO_ROOT}/platform/external/protobuf;protocol=https;branch=master;name=protobuf;destsuffix=git/external/protobuf"
SRC_URI_SCUDO = "git://${TRUSTY_REPO_ROOT}/platform/external/scudo;protocol=https;branch=master;name=scudo;destsuffix=git/external/scudo"
# benhor01: Is libhardware required?
SRC_URI_LIBHARDWARE = "git://${TRUSTY_REPO_ROOT}/platform/hardware/libhardware;protocol=https;branch=master;name=libhardware;destsuffix=git/hardware/libhardware"
SRC_URI_SYS_GATEKEEPER = "git://${TRUSTY_REPO_ROOT}/platform/system/gatekeeper;protocol=https;branch=master;name=sys_gatekeeper;destsuffix=git/system/gatekeeper"
SRC_URI_SYS_ATTESTATION = "git://${TRUSTY_REPO_ROOT}/platform/system/iot/attestation;protocol=https;branch=master;name=sys_attestation;destsuffix=git/system/iot/attestation"
SRC_URI_SYS_KEYMASTER = "git://${TRUSTY_REPO_ROOT}/platform/system/keymaster;protocol=https;branch=master;name=sys_keymaster;destsuffix=git/system/keymaster"
SRC_URI_SYS_TEEUI = "git://${TRUSTY_REPO_ROOT}/platform/system/teeui;protocol=https;branch=master;name=sys_teeui;destsuffix=git/system/teeui"
SRCREV_boringssl = "1f65662f464ec9b476981a5067dc949612e9adde"
SRCREV_freetype = "b6fc4c6f3f84bc55b03b5d5b3f19f7eb12460288"
SRCREV_googletest = "f93adba5c98356dfee63567b65261161c219ad93"
SRCREV_libcppbor = "31258214ed9360e5848889f1b9a68cc5845cc8cb"
SRCREV_libcxx = "bdf5fe7af5f9b2f0427e6aa91763f6ab4b7e2791"
SRCREV_libcxxabi = "ae7de2c7ea5e91282c42505ad17e84ecdc424a7a"
SRCREV_nanopb_c = "fffa37ba95b345f8e485bd5b847372379af3a65c"
SRCREV_open_dice = "87be81258fde167ebcd7ba2749803406b0c3935b"
SRCREV_protobuf = "1523bc6d1ad2933d6c6a5ecdc845e24f0cb6f42b"
SRCREV_scudo = "42ada44cbc291aeb545b06995a3c471dde3faa8e"
SRCREV_libhardware = "ac977b60166c0b7b4dd016b4d15eb35f0c1e14d9"
SRCREV_sys_gatekeeper = "ef308d0a0bcaa9ccdd772b8649c9ec9326dda758"
SRCREV_sys_attestation = "1ce107c299a915d4e4022660ec9be65cb65a653c"
SRCREV_sys_keymaster = "0661671a0498bddce4987f0dbef72926cc8245c0"
SRCREV_sys_teeui = "792b154128b773efae04b0395413bf0752a7a2b8"
# Device
SRC_URI:append = " ${SRC_URI_DEVICE} \
                 "
SRC_URI_DEVICE = "git://${TRUSTY_REPO_ROOT}/trusty/device/arm/generic-arm64;protocol=https;branch=master;name=device;destsuffix=git/trusty/device/arm/generic-arm64"
SRCREV_device = "ab9a1d506198ca92995a3f62cff8792fc298d090"
# Vendor
SRC_URI:append = " ${SRC_URI_VENDOR} \
                 "
SRC_URI_VENDOR = "git://${TRUSTY_REPO_ROOT}/trusty/vendor/google/aosp;protocol=https;branch=master;name=vendor;destsuffix=git/trusty/vendor/google/aosp"
SRCREV_vendor = "3ca32537bdd202fe993c69293f0ef35038bb7203"

S = "${WORKDIR}/git"
B = "${S}/../build-root"

inherit deploy python3native

INHIBIT_DEFAULT_DEPS = "1"

DEPENDS = "coreutils-native zip-native androidclang-native \
           virtual/aarch64-none-elf-gcc-native \
           "

CFLAGS[unexport] = "1"
LDFLAGS[unexport] = "1"
CPPFLAGS[unexport] = "1"
AS[unexport] = "1"
LD[unexport] = "1"
CC[unexport] = "1"

TRUSTY_TARGET ?= "invalid"

do_configure() {
    cp "${S}/external/lk/makefile" "${S}"
    cp "${S}/trusty/vendor/google/aosp/lk_inc.mk" "${S}"
}

do_compile[cleandirs] = "${B}"
do_compile() {
    BUILDID="yocto"
    export BUILDID
    BUILDROOT="${B}"
    export BUILDROOT
    # This path will change when the androidclang recipe is updated.
    # Using the datadir as 'runtimes_ndk_cxx' is found relative
    # to the binary path in lk/engine.mk.
    CLANG_BINDIR="${RECIPE_SYSROOT_NATIVE}/${libexecdir}/clang-r416183b/bin"
    export CLANG_BINDIR
    ARCH_arm64_TOOLCHAIN_PREFIX="${RECIPE_SYSROOT_NATIVE}/${bindir}/aarch64-none-elf-"
    export ARCH_arm64_TOOLCHAIN_PREFIX
    TRUSTY_TOP="${S}"
    export TRUSTY_TOP
    PY3="${RECIPE_SYSROOT_NATIVE}/${bindir}/python3-native/python3"
    export PY3
    oe_runmake -C ${S} "${TRUSTY_TARGET}"
}

do_install() {
   install -d -m 755 ${D}/firmware/
   install -m 0644 "${B}/build-${TRUSTY_TARGET}/lk.bin" "${D}/firmware/lk.bin"
   install -m 0644 "${B}/build-${TRUSTY_TARGET}/lk.elf" "${D}/firmware/lk.elf"
}

SYSROOT_DIRS = "/firmware"

do_deploy() {
    cp -rf ${D}/firmware/* ${DEPLOYDIR}/
}
addtask deploy after do_install

INSANE_SKIP:${PN} = "ldflags buildpaths"

FILES:${PN} = "/firmware/*"
