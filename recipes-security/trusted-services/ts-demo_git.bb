SUMMARY = "Trusted Services demo application"

require ts.inc

PV = "1.0+git${SRCPV}"

inherit python3native cmake

DEPENDS = "python3-pycryptodome-native python3-pycryptodomex-native \
           python3-pyelftools-native python3-grpcio-tools-native \
           python3-protobuf-native protobuf-native nanopb nanopb-native \
           libts \
           "

RDEPENDS:${PN} += "libts"

OECMAKE_SOURCEPATH = "${S}/deployments/ts-demo/arm-linux"
OECMAKE_GENERATOR = "Unix Makefiles"
EXTRA_OECMAKE += "-DCMAKE_POSITION_INDEPENDENT_CODE=True \
                  -DLINUX_FFA_USER_SHIM_INCLUDE_DIR:PATH=/usr/include \
                  -DMBEDTLS_EXTRA_INCLUDES=${STAGING_DIR_HOST}/usr/include \
                  "

do_install:append () {
    install -d ${D}${bindir}
    mv ${D}${TS_INSTALL}/bin/ts-demo ${D}${bindir}

    rm -r --one-file-system ${D}${TS_INSTALL}
}

FILES:${PN} = "${bindir}/ts-demo"

INSANE_SKIP:${PN}-dbg += "buildpaths"
