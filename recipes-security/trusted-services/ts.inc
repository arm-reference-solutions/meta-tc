HOMEPAGE = "https://trusted-services.readthedocs.io/en/latest/index.html"

LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://${S}/license.rst;md5=ea160bac7f690a069c608516b17997f4"
SRC_URI = "git://git.trustedfirmware.org/TS/trusted-services.git;protocol=https;branch=integration;name=ts;destsuffix=git/ts \
           file://0001-Create-a-platform-for-total-compute.patch;patchdir=../ts \
           file://0002-fwu-add-io-drivers-from-tf-a-tests-tagged-v2.7-rc0.patch;patchdir=../ts \
           file://0003-fwu-add-GPT-partition-drivers-from-TF-A-tagged-v2.7-.patch;patchdir=../ts \
           file://0004-fwu-make-the-files-added-from-TF-A-TESTS-to-be-compa.patch;patchdir=../ts \
           file://0005-fwu-make-the-files-added-from-TF-A-to-be-compatible-.patch;patchdir=../ts \
           file://0006-fwu-add-firmware-update-service-components.patch;patchdir=../ts \
           file://0007-fwu-use-image-type-uuid-from-the-gpt-information.patch;patchdir=../ts \
           file://0008-do-not-use-init_info.patch;patchdir=../ts \
           file://0009-crypto-set-platform-to-TC.patch;patchdir=../ts \
           file://0010-add-the-partition-manifest-files.patch;patchdir=../ts \
           file://0011-environment-changes-for-hafnium.patch;patchdir=../ts \
           file://0012-firmware-update-add-new-deployment-for-the-sp-enviro.patch;patchdir=../ts \
           file://0013-firmware-update-remove-shim-layer-dependencies.patch;patchdir=../ts \
           file://0014-fix-update-is_metadata_initialized-before-populating.patch;patchdir=../ts \
           file://0015-Fix-platform-include-when-source-directory-path-may-.patch;patchdir=../ts \
           file://0016-Fix-change-libts-to-export-a-CMake-package.patch;patchdir=../ts \
           file://0017-Adapt-deployments-to-libts-changes.patch;patchdir=../ts \
           "

SRCREV_FORMAT = "ts"
SRCREV_ts = "1b0c520279445fc4d85fc582eda5e5ff5f380c39"

S = "${WORKDIR}/git/ts"

# Default TS installation path
TS_INSTALL = "/usr/arm-linux"

# MbedTLS, tag "mbedtls-3.1.0"
SRC_URI += "git://github.com/ARMmbed/mbedtls.git;name=mbedtls;protocol=https;branch=master;destsuffix=git/mbedtls"
SRCREV_mbedtls = "d65aeb37349ad1a50e0f6c9b694d4b5290d60e49"
LIC_FILES_CHKSUM += "file://../mbedtls/LICENSE;md5=3b83ef96387f14655fc854ddc3c6bd57"

# Nanopb, tag "nanopb-0.4.6"
SRC_URI += "git://github.com/nanopb/nanopb.git;name=nanopb;protocol=https;branch=master;destsuffix=git/nanopb"
SRCREV_nanopb = "afc499f9a410fc9bbf6c9c48cdd8d8b199d49eb4"
LIC_FILES_CHKSUM += "file://../nanopb/LICENSE.txt;md5=9db4b73a55a3994384112efcdb37c01f"

# CppUTest,  tag "v3.8"
SRC_URI += "git://github.com/cpputest/cpputest.git;name=cpputest;protocol=https;branch=master;destsuffix=git/cpputest"
SRCREV_cpputest = "e25097614e1c4856036366877a02346c4b36bb5b"
LIC_FILES_CHKSUM += "file://../cpputest/COPYING;md5=ce5d5f1fe02bcd1343ced64a06fd4177"

EXTRA_OECMAKE += "-DMBEDTLS_SOURCE_DIR=${WORKDIR}/git/mbedtls \
                  -DCPPUTEST_SOURCE_DIR=${WORKDIR}/git/cpputest \
                  -DNANOPB_SOURCE_DIR=${WORKDIR}/git/nanopb \
                 "

# TS cmake files use find_file() to search through source code and build dirs.
# Yocto cmake class limits CMAKE_FIND_ROOT_PATH and find_file() fails.
# Include the source tree and build dirs into searchable path.
OECMAKE_EXTRA_ROOT_PATH = "${WORKDIR}/git/ ${WORKDIR}/build/"

EXTRA_OECMAKE += '-DCROSS_COMPILE="${TARGET_PREFIX}"'
