SRC_URI:append = " \
                 file://cpputest-cmake-fix.patch \
                 "

THIS_DIR := "${THISDIR}"
FILESEXTRAPATHS:prepend = "${THIS_DIR}/${BPN}:"

SRCREV = "e25097614e1c4856036366877a02346c4b36bb5b"

PV = "3.8+git${SRCPV}"

EXTRA_OECMAKE += "-DCMAKE_POSITION_INDEPENDENT_CODE=True \
                 -DMEMORY_LEAK_DETECTION=OFF \
                 -DHAVE_FORK=OFF \
                 -DCPP_PLATFORM=armcc \
                 "
