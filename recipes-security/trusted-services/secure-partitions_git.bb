SUMMARY = "Trusted Services secure partitions"
HOMEPAGE = "https://trusted-services.readthedocs.io/en/latest/index.html"

PACKAGE_ARCH = "${MACHINE_ARCH}"

LICENSE = "Apache-2.0 & BSD-3-Clause & Zlib & CC0-1.0"
LIC_FILES_CHKSUM = "file://license.rst;md5=ea160bac7f690a069c608516b17997f4 \
                    file://../mbedtls/LICENSE;md5=3b83ef96387f14655fc854ddc3c6bd57 \
                    file://../nanopb/LICENSE.txt;md5=9db4b73a55a3994384112efcdb37c01f \
                    "

require ts.inc

SRC_URI:append = " ${SRC_URI_MBED} ${SRC_URI_NANOPB}"

SRC_URI_MBED = "git://github.com/ARMmbed/mbedtls.git;protocol=https;branch=master;name=mbed;destsuffix=git/mbedtls"
SRC_URI_NANOPB = "git://github.com/nanopb/nanopb.git;protocol=https;name=nanopb;branch=master;destsuffix=git/nanopb"

PV = "1.0+git${SRCPV}"

SRCREV_FORMAT = "ts"
SRCREV_nanopb = "afc499f9a410fc9bbf6c9c48cdd8d8b199d49eb4"
SRCREV_mbed = "d65aeb37349ad1a50e0f6c9b694d4b5290d60e49"

# Which environment to create the secure partions for (currently only sp)
TS_ENVIRONMENT ?= "sp"

# Which secure partitions to create
SP_CRYPTO ?= "1"
SP_INTERNAL_TRUSTED_STORAGE ?= "1"
SP_FIRMWARE_UPDATE ?= "1"
TS_DEPLOYMENTS += "${@bb.utils.contains('SP_CRYPTO', '1', 'deployments/crypto/${TS_ENVIRONMENT}', '', d)} \
                   ${@bb.utils.contains('SP_INTERNAL_TRUSTED_STORAGE', '1', 'deployments/internal-trusted-storage/${TS_ENVIRONMENT}', '', d)} \
                   ${@bb.utils.contains('SP_FIRMWARE_UPDATE', '1', 'deployments/firmware-update/${TS_ENVIRONMENT}', '', d)} \
                   "

S = "${WORKDIR}/git/ts"

inherit deploy python3native

INHIBIT_DEFAULT_DEPS = "1"
DEPENDS = "virtual/aarch64-none-elf-gcc-native"

DEPENDS:append = "python3-pycryptodome-native python3-pycryptodomex-native \
           python3-pyelftools-native python3-grpcio-tools-native \
           python3-protobuf-native protobuf-native cmake-native \
           "

DEPENDS:append = " ${@bb.utils.contains('TS_ENVIRONMENT', 'opteesp', 'optee-spdevkit', '', d)}"

B = "${S}/../build"

CFLAGS[unexport] = "1"
LDFLAGS[unexport] = "1"
CPPFLAGS[unexport] = "1"
AS[unexport] = "1"
LD[unexport] = "1"
CC[unexport] = "1"

# only used if TS_ENVIRONMENT is opteesp
SP_DEV_KIT_DIR = "${RECIPE_SYSROOT}/${includedir}/optee/export-user_sp"

do_configure() {
    CROSS_COMPILE="aarch64-none-elf-"
    export CROSS_COMPILE
    if [ "${TS_ENVIRONMENT}" = "opteesp" ]; then
        CFLAGS="-nostdinc \
                -I${SP_DEV_KIT_DIR}/include \
                "
        export CFLAGS
        for TS_DEPLOYMENT in ${TS_DEPLOYMENTS}; do
            cmake \
              -DCMAKE_INSTALL_PREFIX=${D}/firmware/sp \
              -DMBEDTLS_URL=${S}/../mbedtls \
              -DNANOPB_URL=${S}/../nanopb \
              -DSP_DEV_KIT_DIR=${SP_DEV_KIT_DIR} \
              -S ${S}/$TS_DEPLOYMENT -B "${B}/$TS_DEPLOYMENT"
        done
    else
        CFLAGS="-mgeneral-regs-only \
                "
        export CFLAGS
        for TS_DEPLOYMENT in ${TS_DEPLOYMENTS}; do
            cd ${S}/$TS_DEPLOYMENT
            cmake \
              -DPLAT=tc \
              -DNANOPB_URL=${S}/../nanopb \
              -DMBEDTLS_URL=${S}/../mbedtls \
              -S . -B build
        done
    fi
}
do_configure[network] = "1"

do_compile() {
    for TS_DEPLOYMENT in ${TS_DEPLOYMENTS}; do
        cmake --build "${S}/$TS_DEPLOYMENT/build"
    done
}

do_install () {
    if [ "${TS_ENVIRONMENT}" = "opteesp" ]; then
        for TS_DEPLOYMENT in ${TS_DEPLOYMENTS}; do
            cmake --install "${B}/$TS_DEPLOYMENT"
        done
    fi
    if [ "${TS_ENVIRONMENT}" = "sp" ]; then
# Using cmake install gives different binaries
        install -d -m 755 ${D}/firmware/
        if [ "${SP_CRYPTO}" = "1" ]; then
            install -m 0644 "${S}/deployments/crypto/sp/crypto.dts" \
                "${D}/firmware/crypto.dts"
            install -m 0644 "${S}/deployments/crypto/sp/build/d9df52d5-16a2-4bb2-9aa4-d26d3b84e8c0.bin" \
                "${D}/firmware/crypto-sp.bin"
        fi
        if [ "${SP_INTERNAL_TRUSTED_STORAGE}" = "1" ]; then
            install -m 0644 \
                "${S}/deployments/internal-trusted-storage/sp/internal-trusted-storage.dts" \
                "${D}/firmware/internal-trusted-storage.dts"
            install -m 0644 \
                "${S}/deployments/internal-trusted-storage/sp/build/dc1eef48-b17a-4ccf-ac8b-dfcff7711b14.bin" \
                "${D}/firmware/internal-trusted-storage.bin"
        fi
        if [ "${SP_FIRMWARE_UPDATE}" = "1" ]; then
            install -m 0644 \
                "${S}/deployments/firmware-update/sp/firmware-update.dts" \
                "${D}/firmware/firmware-update.dts"
            install -m 0644 \
                "${S}/deployments/firmware-update/sp/build/6823a838-1b06-470e-9774-0cce8bfb53fd.bin" \
                "${D}/firmware/firmware-update.bin"
        fi
    fi
}

SYSROOT_DIRS = "/firmware"

do_deploy() {
    cp -rf ${D}/firmware/* ${DEPLOYDIR}/
}
addtask deploy after do_install

INSANE_SKIP:${PN} = "ldflags buildpaths"

FILES:${PN} = "/firmware/*"
