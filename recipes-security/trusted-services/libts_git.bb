SUMMARY = "Trusted services linux library"

require ts.inc

PV = "1.0+git${SRCPV}"

inherit python3native cmake

DEPENDS = "python3-pycryptodome-native python3-pycryptodomex-native \
           python3-pyelftools-native python3-grpcio-tools-native \
           python3-protobuf-native protobuf-native nanopb-native \
           arm-ffa-user arm-ffa-tee \
           "

OECMAKE_SOURCEPATH = "${S}/deployments/libts/arm-linux"
OECMAKE_GENERATOR = "Unix Makefiles"
EXTRA_OECMAKE += "-DCMAKE_POSITION_INDEPENDENT_CODE=True \
                  -DCMAKE_SYSTEM_NAME=Linux \
                  -DCMAKE_SYSTEM_PROCESSOR=arm \
                  -DLINUX_FFA_USER_SHIM_INCLUDE_DIR:PATH=/usr/include \
                 "

do_install:append () {
    # Move the dynamic libraries into the standard place.
    install -d ${D}${libdir}
    mv ${D}${TS_INSTALL}/lib/libts* ${D}${libdir}
}

FILES:${PN} = "${libdir}/libts.so.*"
FILES:${PN}-dev = "${TS_INSTALL}/lib/cmake ${TS_INSTALL}/include ${libdir}/libts.so"

INSANE_SKIP:${PN}-dev += "buildpaths"
