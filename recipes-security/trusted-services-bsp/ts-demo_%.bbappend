# Machine specific configurations

MACHINE_TS_REQUIRE ?= ""
MACHINE_TS_REQUIRE_tc0 = "ts-tc.inc"
MACHINE_TS_REQUIRE_tc1 = "ts-tc.inc"

require ${MACHINE_TS_REQUIRE}
