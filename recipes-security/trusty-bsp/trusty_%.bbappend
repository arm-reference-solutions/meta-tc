FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI:append = "file://0001-build-fix-Allow-setting-GIC_VERSION-4-and-GIC600-1.patch;patchdir=external/lk \
                  file://0002-Save-the-kernel-base-address-after-kaslr.patch;patchdir=external/lk \
                  file://0003-Yocto-build-fix-Allow-plus-sign-in-build-directory.patch;patchdir=external/lk \
                  file://0004-Yocto-build-fix-Hardcode-KERNEL_LIBC_RANDSEED_HEX.patch;patchdir=external/lk \
                  file://0001-WIP-hafnium-para-virt-fixes-lk.patch;patchdir=external/lk \
                  file://0001-lib-smc-Split-secure-monitor-call-implementation-int.patch;patchdir=trusty/kernel \
                  file://0002-lib-smc-Move-smc-architectural-defines-to-the-smc-mo.patch;patchdir=trusty/kernel \
                  file://0003-lib-arm_ffa-Create-an-FF-A-module.patch;patchdir=trusty/kernel \
                  file://0004-lib-arm_ffa-Initialize-arm_ffa-independently-from-sh.patch;patchdir=trusty/kernel \
                  file://0005-lib-arm_ffa-Get-the-local-FF-A-partition-id.patch;patchdir=trusty/kernel \
                  file://0006-lib-arm_ffa-Check-supported-features-by-function-id.patch;patchdir=trusty/kernel \
                  file://0007-lib-sm-Log-and-fix-cleanup-when-allocating-rx-tx-FF-.patch;patchdir=trusty/kernel \
                  file://0008-lib-arm_ffa-Set-up-the-rx-tx-buffers.patch;patchdir=trusty/kernel \
                  file://0009-lib-arm_ffa-Missing-FF-A-error-code-and-flag-defines.patch;patchdir=trusty/kernel \
                  file://0010-lib-arm_ffa-Add-ffa_mem_relinquish.patch;patchdir=trusty/kernel \
                  file://0011-lib-arm_ffa-Make-FF-A-calls-for-memory-retrieval.patch;patchdir=trusty/kernel \
                  file://0012-lib-arm_ffa-Move-error-defines-to-a-new-header.patch;patchdir=trusty/kernel \
                  file://0013-lib-arm_ffa-Add-missing-FF-A-call-defines-for-direct.patch;patchdir=trusty/kernel \
                  file://0014-lib-arm_ffa-Get-and-set-for-inbound-FF-A-calls.patch;patchdir=trusty/kernel \
                  file://0015-lib-arm_ffa-FF-A-error-response.patch;patchdir=trusty/kernel \
                  file://0016-lib-arm_ffa-Support-for-FFA_MSG_WAIT.patch;patchdir=trusty/kernel \
                  file://0017-lib-arm_ffa-Interface-to-send-partition-msg-as-direc.patch;patchdir=trusty/kernel \
                  file://0018-lib-sm-New-api-in-which-FF-A-messages-carry-trusty-s.patch;patchdir=trusty/kernel \
                  file://0019-lib-arm_ffa-register-secondary-core-entry-point.patch;patchdir=trusty/kernel \
                  file://0020-spmc-S-EL2-Use-CNTHPS-interrupt-and-avoid-access-to-.patch;patchdir=trusty/kernel \
                  file://0021-yocto-build-fix-don-t-use-envsetup.sh.patch;patchdir=trusty/kernel \
                  file://0022-spmc-S-EL2-Get-the-cpu-num.patch;patchdir=trusty/kernel \
                  file://0001-WIP-hafnium-para-virt-fixes-trusty-kernel.patch;patchdir=trusty/kernel \
                  file://0001-build-fix-don-t-use-envsetup.sh.patch;patchdir=trusty/user/base \
                  file://0001-spmc-S-EL2-Disable-tests-using-timeouts.patch;patchdir=trusty/user/base \
                  file://0001-Remove-apps-that-depend-on-rust.patch;patchdir=trusty/user/app/sample \
                  file://0001-Add-the-total-compute-platform.patch;patchdir=trusty/device/arm/generic-arm64 \
                  "

TRUSTY_TARGET = "tc-test-debug"
