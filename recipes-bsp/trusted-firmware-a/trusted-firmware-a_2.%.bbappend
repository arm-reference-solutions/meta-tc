# Secure partitions configuration for tfa on tc platforms

# Also build a fip with trusty in it
DEPENDS += "tfa_trusty"

FILESEXTRAPATHS:prepend:tc0 := "${THISDIR}/files/tc:"
FILESEXTRAPATHS:prepend:tc1 := "${THISDIR}/files/tc:"

DEPENDS += "secure-partitions"
SRC_URI:append = " file://sp_layout.json;subdir=sp_layout"

TFA_SP_LAYOUT_FILE = "${WORKDIR}/sp_layout/sp_layout.json"
TFA_ARM_SPMC_MANIFEST_DTS = "plat/arm/board/tc/fdts/tc_spmc_optee_sp_manifest.dts"

EXTRA_OEMAKE += "TS_SP_FW_CONFIG=1"

do_compile:prepend() {
    cp -t ${WORKDIR}/sp_layout \
        ${RECIPE_SYSROOT}/${nonarch_base_libdir}/firmware/tee-pager_v2.bin \
        ${RECIPE_SYSROOT}/${nonarch_base_libdir}/firmware/optee_sp_manifest.dts \
        ${RECIPE_SYSROOT}/firmware/internal-trusted-storage.bin \
        ${RECIPE_SYSROOT}/firmware/internal-trusted-storage.dts \
        ${RECIPE_SYSROOT}/firmware/crypto-sp.bin \
        ${RECIPE_SYSROOT}/firmware/crypto.dts \
        ${RECIPE_SYSROOT}/firmware/firmware-update.dts \
        ${RECIPE_SYSROOT}/firmware/firmware-update.bin
}
