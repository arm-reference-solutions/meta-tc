require recipes-bsp/trusted-firmware-a/trusted-firmware-a_2.7.0.bb
require recipes-bsp/trusted-firmware-a/trusted-firmware-a-tc.inc

PROVIDES = "tfa_trusty"

FILESEXTRAPATHS:prepend := "${THISDIR}/files/tc:"

DEPENDS += "trusty"
SRC_URI:append = "file://sp_layout_trusty.json \
                  file://tc_trusty_sp_manifest.dts;subdir=git/plat/arm/board/tc/fdts \
                  "

TFA_SP_LAYOUT_FILE = "${WORKDIR}/sp_layout/sp_layout_trusty.json"
TFA_ARM_SPMC_MANIFEST_DTS = "plat/arm/board/tc/fdts/tc_spmc_trusty_sp_manifest.dts"

EXTRA_OEMAKE:remove = "PSA_FWU_SUPPORT=1 ARM_GPT_SUPPORT=1"

do_compile[cleandirs] += "${WORKDIR}/sp_layout"
do_compile:prepend() {
     cp -t ${WORKDIR}/sp_layout \
         ${RECIPE_SYSROOT}/firmware/lk.bin \
         ${S}/plat/arm/board/tc/fdts/tc_trusty_sp_manifest.dts \
         ${WORKDIR}/sp_layout_trusty.json
}

deltask do_generate_gpt

# Workaround existing do_install():append
addtask do_tfa_install after do_compile before do_install
do_install[noexec] = "1"

fakeroot do_tfa_install() {
    install -d -m 755 ${D}/firmware-trusty
    install -m 0644 ${BUILD_DIR}/fip.bin \
                ${D}/firmware-trusty/fip-trusty-${TFA_PLATFORM}.bin
    install -m 0644 ${BUILD_DIR}/bl1.bin \
                ${D}/firmware-trusty/bl1-trusty-${TFA_PLATFORM}.bin
}
do_tfa_install[depends] = "virtual/fakeroot-native:do_populate_sysroot"

FILES:${PN} = "/firmware-trusty"
SYSROOT_DIRS = "/firmware-trusty"

do_deploy() {
    cp -rf ${D}/firmware-trusty/* ${DEPLOYDIR}/
}
